export declare class CreateServiceDto {
    title: string;
    slug: string;
    miniDescription: string;
    description: string;
    mainProductImage: object;
}
