import { ImageProxyQuery } from "./utils/dto/ImageProxy.query";
import { AppService } from "./app.service";
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    serveCompanyImages(fileId: any, res: any, req: any, query: ImageProxyQuery): Promise<any>;
    serveCompanyAdvantagesImages(fileId: any, res: any, req: any, query: ImageProxyQuery): Promise<any>;
    serveOrganizationImages(fileId: any, res: any, req: any, query: ImageProxyQuery): Promise<any>;
    serveServiceImages(fileId: any, res: any, req: any, query: ImageProxyQuery): Promise<any>;
    servePhotoAlbumImages(fileId: any, res: any, req: any, query: ImageProxyQuery): Promise<any>;
    serveBlogImages(fileId: any, res: any, req: any, query: ImageProxyQuery): Promise<any>;
    handleCron(): Promise<void>;
}
