import {Module} from '@nestjs/common';
import {ServicesService} from './services.service';
import {ServicesController} from './services.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {UploadController} from "../upload/upload.controller";
import {ErrorsService} from "../../config/errors.service";
import {UploadService} from "../upload/upload.service";
import {ServicesSchema} from "../../schemas/services.schema";

@Module({
    imports: [MongooseModule.forFeature([{name: 'Services', schema: ServicesSchema}])],
    controllers: [ServicesController, UploadController],
    providers: [ServicesService, ErrorsService, UploadService]
})
export class ServicesModule {
}
