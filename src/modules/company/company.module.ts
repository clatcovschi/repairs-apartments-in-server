import {Module} from '@nestjs/common';
import {CompanyService} from './company.service';
import {CompanyController} from './company.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {CompanySchema} from "../../schemas/company.schema";
import {ErrorsService} from "../../config/errors.service";
import {UploadController} from "../upload/upload.controller";
import {UploadService} from "../upload/upload.service";

@Module({
    imports: [MongooseModule.forFeature([{name: 'Company', schema: CompanySchema}])],
    controllers: [CompanyController, UploadController],
    providers: [CompanyService, ErrorsService, UploadService]
})
export class CompanyModule {
}
