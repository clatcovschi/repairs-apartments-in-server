"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GoalsSchema = void 0;
const mongoose_1 = require("mongoose");
const slug = require("mongoose-slug-updater");
const mongoosePaginate = require("mongoose-paginate-v2");
exports.GoalsSchema = new mongoose_1.Schema({
    title: { type: String },
    goals: [],
}, {
    minimize: false,
    timestamps: true,
});
exports.GoalsSchema.plugin(slug);
exports.GoalsSchema.plugin(mongoosePaginate);
//# sourceMappingURL=goals.schema.js.map