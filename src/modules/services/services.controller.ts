import {Controller, Get, Post, Body, Patch, Param, Delete, Query, UseGuards, Put} from '@nestjs/common';
import { ServicesService } from './services.service';
import { CreateServiceDto } from './dto/create-service.dto';
import {ApiBearerAuth, ApiTags} from "@nestjs/swagger";
import {AuthGuard} from "@nestjs/passport";
import {Roles} from "../../decorators/roles.decorator";
import {ServicesSchema} from "../../schemas/services.schema";

@ApiTags('Services')
@Controller('services')
export class ServicesController {
  constructor(private readonly service: ServicesService) {
  }

  @Get()
  async findAll(@Query() query: any): Promise<ServicesSchema[]> {
    return this.service.findAll(query);
  }

  @Get('/:id')
  async findById(@Param('id') id: string): Promise<ServicesSchema> {
    return this.service.findById(id);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Body() createProductDto: CreateServiceDto) {
    return this.service.create(createProductDto);
  }

  @ApiBearerAuth()
  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findByIdAndUpdate(@Param('id') id: string, @Body() product): Promise<ServicesSchema> {
    return this.service.findByIdAndUpdate(id, product);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.service.remove(id);
  }

}
