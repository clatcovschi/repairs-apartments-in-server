"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileController = void 0;
const common_1 = require("@nestjs/common");
const users_service_1 = require("../users/users.service");
const profile_service_1 = require("./profile.service");
const update_profile_dto_1 = require("./dto/update-profile.dto");
const create_profile_dto_1 = require("./dto/create-profile.dto");
const swagger_1 = require("@nestjs/swagger");
let ProfileController = class ProfileController {
    constructor(userService, profileService) {
        this.userService = userService;
        this.profileService = profileService;
    }
    async activateEmail(email, token) {
        return this.profileService.activateEmailByToken(email, token);
    }
    async resetPasswordInit(resetPasswordDto) {
        return this.profileService.resetPasswordInit(resetPasswordDto.email);
    }
    async resetPasswordFinish(resetPasswordFinishDto) {
        return this.profileService.resetPasswordFinish(resetPasswordFinishDto);
    }
};
__decorate([
    common_1.Get('/activate-email'),
    __param(0, common_1.Query('email')),
    __param(1, common_1.Query('key')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], ProfileController.prototype, "activateEmail", null);
__decorate([
    common_1.Post('/reset-password/init'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_profile_dto_1.UpdateProfileDto]),
    __metadata("design:returntype", Promise)
], ProfileController.prototype, "resetPasswordInit", null);
__decorate([
    common_1.Post('/reset-password/finish'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_profile_dto_1.CreateProfileDto]),
    __metadata("design:returntype", Promise)
], ProfileController.prototype, "resetPasswordFinish", null);
ProfileController = __decorate([
    swagger_1.ApiTags('Profile'),
    common_1.Controller('profile'),
    __metadata("design:paramtypes", [users_service_1.UsersService, profile_service_1.ProfileService])
], ProfileController);
exports.ProfileController = ProfileController;
//# sourceMappingURL=profile.controller.js.map