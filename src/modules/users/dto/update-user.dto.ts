import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateUserDto {
    @ApiPropertyOptional()
    readonly firstName: string;
    @ApiPropertyOptional()
    readonly lastName: string;
    @ApiProperty()
    readonly description: string;
}
