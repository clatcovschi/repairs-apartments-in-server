import {Injectable} from '@nestjs/common';
import {CreatePhotoAlbumDto} from './dto/create-photo-album.dto';
import {InjectModel} from "@nestjs/mongoose";
import {PaginateModel} from "mongoose-paginate-v2"
import {PhotoAlbumsSchema} from "../../schemas/photo-albums.schema";

@Injectable()
export class PhotoAlbumsService {
    constructor(@InjectModel('PhotoAlbums') private model: PaginateModel<PhotoAlbumsSchema>) {
    }

    async findById(id: string): Promise<PhotoAlbumsSchema> {
        return await this.model.findById(id).lean();
    }

    async create(createProductDto: CreatePhotoAlbumDto): Promise<PhotoAlbumsSchema> {
        return this.model.create(createProductDto);
    }

    async findByIdAndUpdate(id: string, product): Promise<PhotoAlbumsSchema> {
        return this.model.findByIdAndUpdate(id, product);
    }


    async findAll(queryParams: any): Promise<PhotoAlbumsSchema[]> {
        const query: any = {};
        const options: any = {
            lean: true,
            page: parseInt(queryParams.page, 10) || 1,
            limit: parseInt(queryParams.limit, 10) || 10,
            sort: {createdAt: -1},
        };

        return this.model.paginate(query, options);
    }

    async remove(id: string): Promise<PhotoAlbumsSchema> {
        return this.model.findByIdAndDelete(id);
    }
}
