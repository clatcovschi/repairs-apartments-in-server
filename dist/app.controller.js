"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const ImageProxy_query_1 = require("./utils/dto/ImageProxy.query");
const schedule_1 = require("@nestjs/schedule");
const app_service_1 = require("./app.service");
let AppController = class AppController {
    constructor(appService) {
        this.appService = appService;
    }
    async serveCompanyImages(fileId, res, req, query) {
        res.sendFile(fileId, { root: 'public/company/images' });
    }
    async serveCompanyAdvantagesImages(fileId, res, req, query) {
        res.sendFile(fileId, { root: 'public/company-advantages/images' });
    }
    async serveOrganizationImages(fileId, res, req, query) {
        res.sendFile(fileId, { root: 'public/organization/images' });
    }
    async serveServiceImages(fileId, res, req, query) {
        res.sendFile(fileId, { root: 'public/service/images' });
    }
    async servePhotoAlbumImages(fileId, res, req, query) {
        res.sendFile(fileId, { root: 'public/photo-album/images' });
    }
    async serveBlogImages(fileId, res, req, query) {
        res.sendFile(fileId, { root: 'public/blog/images' });
    }
    async handleCron() {
        const filterEntities = [['company', 'company', true, false, false], ['company-advantages', 'company-advantages', false, false, false], ['blog', 'blog', false, false, false], ['services', 'service', false, false, false], ['organization', 'organization', false, true, false], ['photo-albums', 'photo-album', false, false, true]];
        filterEntities.map(el => this.appService.filterImages(el[0], el[1], el[2], el[3], el[4]));
    }
};
__decorate([
    common_1.Get('public/company/images/:fileId'),
    __param(0, common_1.Param('fileId')),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __param(3, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, ImageProxy_query_1.ImageProxyQuery]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "serveCompanyImages", null);
__decorate([
    common_1.Get('public/company-advantages/images/:fileId'),
    __param(0, common_1.Param('fileId')),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __param(3, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, ImageProxy_query_1.ImageProxyQuery]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "serveCompanyAdvantagesImages", null);
__decorate([
    common_1.Get('public/organization/images/:fileId'),
    __param(0, common_1.Param('fileId')),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __param(3, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, ImageProxy_query_1.ImageProxyQuery]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "serveOrganizationImages", null);
__decorate([
    common_1.Get('public/service/images/:fileId'),
    __param(0, common_1.Param('fileId')),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __param(3, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, ImageProxy_query_1.ImageProxyQuery]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "serveServiceImages", null);
__decorate([
    common_1.Get('public/photo-album/images/:fileId'),
    __param(0, common_1.Param('fileId')),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __param(3, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, ImageProxy_query_1.ImageProxyQuery]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "servePhotoAlbumImages", null);
__decorate([
    common_1.Get('public/blog/images/:fileId'),
    __param(0, common_1.Param('fileId')),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __param(3, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, ImageProxy_query_1.ImageProxyQuery]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "serveBlogImages", null);
__decorate([
    schedule_1.Cron(schedule_1.CronExpression.EVERY_WEEK),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AppController.prototype, "handleCron", null);
AppController = __decorate([
    swagger_1.ApiTags('Public'),
    common_1.Controller(),
    __metadata("design:paramtypes", [app_service_1.AppService])
], AppController);
exports.AppController = AppController;
//# sourceMappingURL=app.controller.js.map