import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from '../../schemas/user.schema';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { ErrorsService } from '../../config/errors.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'User', schema: UserSchema }])],
  controllers: [UsersController],
  providers: [UsersService, ErrorsService],
  exports: [UsersService],
})
export class UsersModule {}
