export declare class CreatePerfectRenovationDto {
    title: string;
    renovation: object;
}
