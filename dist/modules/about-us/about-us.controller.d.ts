import { AboutUsService } from './about-us.service';
import { CreateAboutUsDto } from './dto/create-about-us.dto';
import { AboutUsSchema } from "../../schemas/about-us.schema";
export declare class AboutUsController {
    private readonly service;
    constructor(service: AboutUsService);
    findAll(query: any): Promise<AboutUsSchema[]>;
    findById(id: string): Promise<AboutUsSchema>;
    create(createProductDto: CreateAboutUsDto): Promise<AboutUsSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<AboutUsSchema>;
    remove(id: string): Promise<AboutUsSchema>;
}
