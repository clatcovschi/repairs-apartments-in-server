import {ApiProperty} from '@nestjs/swagger';
export class CreateCompanyDto {
    @ApiProperty()
    name: string;
    @ApiProperty()
    phone: string;
    @ApiProperty()
    slug: string;
    @ApiProperty()
    mail: string;
    @ApiProperty()
    coordinates: string;
    @ApiProperty()
    networksDescription: string;
    @ApiProperty()
    whatsapp: string;
    @ApiProperty()
    whatsappDescription: string;
    @ApiProperty()
    vk: string;
    @ApiProperty()
    instagram: string;
    @ApiProperty()
    facebook: string;
    @ApiProperty()
    workingHours: string;
    @ApiProperty()
    principalColor: string;
    @ApiProperty()
    secondaryColor: string;
    @ApiProperty()
    slogan: string;
    @ApiProperty()
    servicePageDescription: string;
    @ApiProperty()
    photoAlbumsPageDescription: string;
    @ApiProperty()
    mainProductImage: object;
    @ApiProperty()
    mainProductImageContacts: object;

}

