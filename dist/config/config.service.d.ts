export declare class ConfigService {
    MONGODB_URI: string;
    private readonly envConfig;
    constructor();
    get(key: string): string;
}
