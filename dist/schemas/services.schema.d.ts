import { Document, Schema } from 'mongoose';
export interface ServicesSchema extends Document {
    title: string;
    slug: string;
    miniDescription: string;
    description: string;
    mainProductImage: object;
}
export declare const ServicesSchema: Schema<Document<any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
