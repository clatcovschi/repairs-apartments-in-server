import {Controller, Get, Post, Body, Patch, Param, Delete, Query, UseGuards, Put} from '@nestjs/common';
import {GoalsService} from './goals.service';
import {CreateGoalDto} from './dto/create-goal.dto';
import {ApiBearerAuth, ApiTags} from "@nestjs/swagger";
import {AuthGuard} from "@nestjs/passport";
import {Roles} from "../../decorators/roles.decorator";
import {GoalsSchema} from "../../schemas/goals.schema";

@ApiTags('Goals')
@Controller('goals')
export class GoalsController {
    constructor(private readonly service: GoalsService) {
    }

    @Get()
    async findAll(@Query() query: any): Promise<GoalsSchema[]> {
        return this.service.findAll(query);
    }

    @Get('/:id')
    async findById(@Param('id') id: string): Promise<GoalsSchema> {
        return this.service.findById(id);
    }

    @ApiBearerAuth()
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async create(@Body() createProductDto: CreateGoalDto) {
        return this.service.create(createProductDto);
    }

    @ApiBearerAuth()
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async findByIdAndUpdate(@Param('id') id: string, @Body() product): Promise<GoalsSchema> {
        return this.service.findByIdAndUpdate(id, product);
    }

    @ApiBearerAuth()
    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async remove(@Param('id') id: string) {
        return this.service.remove(id);
    }
}
