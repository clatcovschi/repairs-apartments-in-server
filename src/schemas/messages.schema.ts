import {Document, Schema} from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface MessagesSchema extends Document {
    name: string;
    phone: string;
    mail: string;
    message: string;
}

export const MessagesSchema = new Schema(
    {
        name: {type: String},
        phone: {type: String},
        mail: {type: String},
        message: {type: String},
    },
    {
        minimize: false,
        timestamps: true,
    },
);

MessagesSchema.plugin(slug);
MessagesSchema.plugin(mongoosePaginate);
