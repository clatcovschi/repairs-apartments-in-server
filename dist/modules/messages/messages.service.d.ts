import { CreateMessageDto } from './dto/create-message.dto';
import { PaginateModel } from "mongoose-paginate-v2";
import { MessagesSchema } from "../../schemas/messages.schema";
import { EmailService } from "../email/email.service";
export declare class MessagesService {
    private model;
    private readonly email;
    constructor(model: PaginateModel<MessagesSchema>, email: EmailService);
    findById(id: string): Promise<MessagesSchema>;
    create(createProductDto: CreateMessageDto): Promise<MessagesSchema>;
    sendEmail(createProductDto: CreateMessageDto): Promise<void>;
    findByIdAndUpdate(id: string, product: any): Promise<MessagesSchema>;
    findAll(queryParams: any): Promise<MessagesSchema[]>;
    remove(id: string): Promise<MessagesSchema>;
}
