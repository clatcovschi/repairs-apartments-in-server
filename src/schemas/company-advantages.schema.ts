import {Document, Schema} from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface CompanyAdvantagesSchema extends Document {
    title: string;
    description: string;
    mainProductImage: object,
}

export const CompanyAdvantagesSchema = new Schema(
    {
        title: {type: String},
        description: {type: String},
        mainProductImage: {
            description: String,
            path: {type: String, default: null},
            mimetype: {type: String, default: null},
            updatedAt: {type: Date, default: Date.now},
        },
    },
    {
        minimize: false,
        timestamps: true,
    },
);

CompanyAdvantagesSchema.plugin(slug);
CompanyAdvantagesSchema.plugin(mongoosePaginate);
