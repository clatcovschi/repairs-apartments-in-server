"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppService = void 0;
const axios_1 = require("@nestjs/axios");
const config_service_1 = require("./config/config.service");
const fs = require("fs");
const common_1 = require("@nestjs/common");
let AppService = class AppService {
    constructor(httpService, config) {
        this.httpService = httpService;
        this.config = config;
    }
    filterImages(path, folder, company, organization, album) {
        this.httpService.get(`${this.config.get('API_URL')}/api/${path}`).subscribe(data => {
            let src = [];
            let images = [];
            fs.readdir(`public/${folder}/images`, (err, files) => {
                let toggle = false;
                if (album) {
                    data.data.docs.map(album => {
                        album.albums.map(image => {
                            images.push(image);
                        });
                    });
                }
                src = organization ? data.data.docs[0].organizations : album ? images : data.data.docs;
                files.forEach(file => {
                    if (company) {
                        if (file !== data.data.docs[0].mainProductImage.description && file !== data.data.docs[0].mainProductImageContacts.description) {
                            console.log('Deleted unused image');
                            fs.unlinkSync('public/company/images/' + file);
                        }
                    }
                    else {
                        src.map(image => file === image.mainProductImage.description ? toggle = true : '');
                        if (toggle) {
                            toggle = false;
                        }
                        else {
                            console.log('Deleted unused image');
                            fs.unlinkSync(`public/${folder}/images/${file}`);
                        }
                    }
                });
            });
        });
    }
};
AppService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [axios_1.HttpService, config_service_1.ConfigService])
], AppService);
exports.AppService = AppService;
//# sourceMappingURL=app.service.js.map