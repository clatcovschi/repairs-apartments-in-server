export declare class CreateOrganizationDto {
    title: string;
    organizations: object;
    description: string;
}
