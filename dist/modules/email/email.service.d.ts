import { ConfigService } from '../../config/config.service';
import { SendGridService } from "@ntegral/nestjs-sendgrid";
export declare class EmailService {
    private readonly sendGrid;
    private readonly config;
    constructor(sendGrid: SendGridService, config: ConfigService);
    sendEmail(obj: any): Promise<void>;
}
