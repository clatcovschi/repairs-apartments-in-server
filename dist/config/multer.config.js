"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogOptions = exports.PhotoAlbumOptions = exports.ServiceOptions = exports.OrganizationOptions = exports.CompanyAdvantagesOptions = exports.CompanyOptions = exports.fileSizeLimits = exports.multerConfig = void 0;
const path_1 = require("path");
const fs_1 = require("fs");
const multer_1 = require("multer");
const uuid_1 = require("uuid");
const common_1 = require("@nestjs/common");
exports.multerConfig = {
    CompanyUpload: './public/company/images/',
    CompanyAdvantagesUpload: './public/company-advantages/images/',
    OrganizationUpload: './public/organization/images/',
    ServiceUpload: './public/service/images/',
    PhotoAlbumUpload: './public/photo-album/images/',
    BlogUpload: './public/blog/images/',
};
exports.fileSizeLimits = {
    images: 10 * 1024 * 1024,
    documents: 25 * 1024 * 1024,
};
exports.CompanyOptions = {
    limits: {
        fileSize: exports.fileSizeLimits.images,
    },
    fileFilter: (req, file, cb) => {
        if (file.mimetype.match(/\/(jpeg|png|gif|svg\+xml|x-icon|webp)$/)) {
            cb(null, true);
        }
        else {
            cb(new common_1.HttpException(`Unsupported file type ${path_1.extname(file.originalname)}`, common_1.HttpStatus.BAD_REQUEST), false);
        }
    },
    storage: multer_1.diskStorage({
        destination: (req, file, cb) => {
            const uploadPath = exports.multerConfig.CompanyUpload;
            if (!fs_1.existsSync(uploadPath)) {
                fs_1.mkdirSync(uploadPath, { recursive: true });
            }
            cb(null, uploadPath);
        },
        filename: (req, file, cb) => {
            cb(null, `${uuid_1.v4()}${path_1.extname(file.originalname)}`);
        },
    }),
};
exports.CompanyAdvantagesOptions = {
    limits: {
        fileSize: exports.fileSizeLimits.images,
    },
    fileFilter: (req, file, cb) => {
        if (file.mimetype.match(/\/(jpeg|png|gif|svg\+xml|x-icon|webp)$/)) {
            cb(null, true);
        }
        else {
            cb(new common_1.HttpException(`Unsupported file type ${path_1.extname(file.originalname)}`, common_1.HttpStatus.BAD_REQUEST), false);
        }
    },
    storage: multer_1.diskStorage({
        destination: (req, file, cb) => {
            const uploadPath = exports.multerConfig.CompanyAdvantagesUpload;
            if (!fs_1.existsSync(uploadPath)) {
                fs_1.mkdirSync(uploadPath, { recursive: true });
            }
            cb(null, uploadPath);
        },
        filename: (req, file, cb) => {
            cb(null, `${uuid_1.v4()}${path_1.extname(file.originalname)}`);
        },
    }),
};
exports.OrganizationOptions = {
    limits: {
        fileSize: exports.fileSizeLimits.images,
    },
    fileFilter: (req, file, cb) => {
        if (file.mimetype.match(/\/(jpeg|png|gif|svg\+xml|x-icon|webp)$/)) {
            cb(null, true);
        }
        else {
            cb(new common_1.HttpException(`Unsupported file type ${path_1.extname(file.originalname)}`, common_1.HttpStatus.BAD_REQUEST), false);
        }
    },
    storage: multer_1.diskStorage({
        destination: (req, file, cb) => {
            const uploadPath = exports.multerConfig.OrganizationUpload;
            if (!fs_1.existsSync(uploadPath)) {
                fs_1.mkdirSync(uploadPath, { recursive: true });
            }
            cb(null, uploadPath);
        },
        filename: (req, file, cb) => {
            cb(null, `${uuid_1.v4()}${path_1.extname(file.originalname)}`);
        },
    }),
};
exports.ServiceOptions = {
    limits: {
        fileSize: exports.fileSizeLimits.images,
    },
    fileFilter: (req, file, cb) => {
        if (file.mimetype.match(/\/(jpeg|png|gif|svg\+xml|x-icon|webp)$/)) {
            cb(null, true);
        }
        else {
            cb(new common_1.HttpException(`Unsupported file type ${path_1.extname(file.originalname)}`, common_1.HttpStatus.BAD_REQUEST), false);
        }
    },
    storage: multer_1.diskStorage({
        destination: (req, file, cb) => {
            const uploadPath = exports.multerConfig.ServiceUpload;
            if (!fs_1.existsSync(uploadPath)) {
                fs_1.mkdirSync(uploadPath, { recursive: true });
            }
            cb(null, uploadPath);
        },
        filename: (req, file, cb) => {
            cb(null, `${uuid_1.v4()}${path_1.extname(file.originalname)}`);
        },
    }),
};
exports.PhotoAlbumOptions = {
    limits: {
        fileSize: exports.fileSizeLimits.images,
    },
    fileFilter: (req, file, cb) => {
        if (file.mimetype.match(/\/(jpeg|png|gif|svg\+xml|x-icon|webp)$/)) {
            cb(null, true);
        }
        else {
            cb(new common_1.HttpException(`Unsupported file type ${path_1.extname(file.originalname)}`, common_1.HttpStatus.BAD_REQUEST), false);
        }
    },
    storage: multer_1.diskStorage({
        destination: (req, file, cb) => {
            const uploadPath = exports.multerConfig.PhotoAlbumUpload;
            if (!fs_1.existsSync(uploadPath)) {
                fs_1.mkdirSync(uploadPath, { recursive: true });
            }
            cb(null, uploadPath);
        },
        filename: (req, file, cb) => {
            cb(null, `${uuid_1.v4()}${path_1.extname(file.originalname)}`);
        },
    }),
};
exports.BlogOptions = {
    limits: {
        fileSize: exports.fileSizeLimits.images,
    },
    fileFilter: (req, file, cb) => {
        if (file.mimetype.match(/\/(jpeg|png|gif|svg\+xml|x-icon|webp)$/)) {
            cb(null, true);
        }
        else {
            cb(new common_1.HttpException(`Unsupported file type ${path_1.extname(file.originalname)}`, common_1.HttpStatus.BAD_REQUEST), false);
        }
    },
    storage: multer_1.diskStorage({
        destination: (req, file, cb) => {
            const uploadPath = exports.multerConfig.BlogUpload;
            if (!fs_1.existsSync(uploadPath)) {
                fs_1.mkdirSync(uploadPath, { recursive: true });
            }
            cb(null, uploadPath);
        },
        filename: (req, file, cb) => {
            cb(null, `${uuid_1.v4()}${path_1.extname(file.originalname)}`);
        },
    }),
};
//# sourceMappingURL=multer.config.js.map