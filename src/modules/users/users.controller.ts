import {Body, Controller, Get, Param, Post, Put, Delete, Query, UseGuards} from '@nestjs/common';
import {UsersService} from './users.service';
import {CreateUserDto} from './dto/create-user.dto';
import {User} from '../../schemas/user.schema';
import {AuthGuard} from '@nestjs/passport';
import {AuthUser} from '../../decorators/user.decorator';
import {Roles} from '../../decorators/roles.decorator';
import {ApiBearerAuth, ApiTags} from '@nestjs/swagger';

@ApiTags('User')
@ApiBearerAuth()
@Controller('users')
@UseGuards(AuthGuard('jwt'))
@Roles('admin')
export class UsersController {
    constructor(private readonly usersService: UsersService) {
    }

    @Get('/me')
    async geyMyUserProfile(@AuthUser() user: any): Promise<User> {
        return this.usersService.getMyProfile(user._id);
    }


    @Get('/')
    @Roles('admin')
    async findAll(@Query() query): Promise<User[]> {
        return this.usersService.findAll(query);
    }

    @Get('/:id')
    @Roles('admin')
    async findById(@Param('id') id: string): Promise<User> {
        return this.usersService.findById(id);
    }

    @Post()
    @Roles('admin')
    async create(@Body() createUserDto: CreateUserDto) {
        return this.usersService.create(createUserDto);
    }

    @Delete('/:id')
    @Roles('admin')
    async findByIdAndDelete(@Param('id') id: string): Promise<User> {
        return this.usersService.findByIdAndDelete(id);
    }

}
