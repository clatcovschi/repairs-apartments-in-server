import {Controller, Post, UploadedFile, UseGuards, UseInterceptors} from '@nestjs/common';
import {FileInterceptor} from '@nestjs/platform-express';
import * as multer from '../../config/multer.config';
import {ApiTags} from '@nestjs/swagger';
import {UploadService} from './upload.service';
import {AuthGuard} from '@nestjs/passport';

@ApiTags('Upload')
@Controller('upload')
@UseGuards(AuthGuard('jwt'))
export class UploadController {
    constructor(private uploadService: UploadService) {
    }

    @Post('/company/image')
    @UseInterceptors(FileInterceptor('image', multer.CompanyOptions))
    async uploadCompanyImage(@UploadedFile() file) {
        return file;
    }

    @Post('/company-advantages/image')
    @UseInterceptors(FileInterceptor('image', multer.CompanyAdvantagesOptions))
    async uploadCompanyAdvantagesImage(@UploadedFile() file) {
        return file;
    }

    @Post('/organization/image')
    @UseInterceptors(FileInterceptor('image', multer.OrganizationOptions))
    async uploadOrganizationImage(@UploadedFile() file) {
        return file;
    }

    @Post('/service/image')
    @UseInterceptors(FileInterceptor('image', multer.ServiceOptions))
    async uploadServiceImage(@UploadedFile() file) {
        return file;
    }

    @Post('/photo-album/image')
    @UseInterceptors(FileInterceptor('image', multer.PhotoAlbumOptions))
    async uploadPhotoAlbumImage(@UploadedFile() file) {
        return file;
    }

    @Post('/blog/image')
    @UseInterceptors(FileInterceptor('image', multer.BlogOptions))
    async uploadBlogImage(@UploadedFile() file) {
        return file;
    }
}
