import {Document, Schema} from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface OrganizationSchema extends Document {
    title: string;
    organizations: object;
    description: string
}

export const OrganizationSchema = new Schema(
    {
        title: {type: String},
        organizations: [],
        description: {type: String},
    },
    {
        minimize: false,
        timestamps: true,
    },
);

OrganizationSchema.plugin(slug);
OrganizationSchema.plugin(mongoosePaginate);
