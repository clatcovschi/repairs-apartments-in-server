import {Document, Schema} from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface CompanySchema extends Document {
    name: string;
    slug: string;
    mail: string;
    phone: string;
    coordinates: string;
    networksDescription: string;
    whatsapp: string;
    whatsappDescription: string;
    vk: string;
    instagram?: string;
    facebook?: string;
    workingHours: string;
    principalColor?: string;
    secondaryColor?: string;
    slogan?: string;
    servicePageDescription: string;
    blogPageDescription: string;
    photoAlbumsPageDescription: string;
    mainProductImage: object;
    mainProductImageContacts: object;
}

export const CompanySchema = new Schema(
    {
        name: {type: String},
        phone: {type: String},
        slug: {type: String},
        slogan: {type: String},
        servicePageDescription: {type: String},
        blogPageDescription: {type: String},
        photoAlbumsPageDescription: {type: String},
        principalColor: {type: String},
        secondaryColor: {type: String},
        mail: {type: String},
        coordinates: {type: String},
        networksDescription: {type: String},
        whatsapp: {type: String},
        whatsappDescription: {type: String},
        vk: {type: String},
        instagram: {type: String},
        facebook: {type: String},
        workingHours: {type: String},
        mainProductImage: {
            description: String,
            path: {type: String, default: null},
            mimetype: {type: String, default: null},
            updatedAt: {type: Date, default: Date.now},
        },
        mainProductImageContacts: {
            description: String,
            path: {type: String, default: null},
            mimetype: {type: String, default: null},
            updatedAt: {type: Date, default: Date.now},
        },
    },
    {
        minimize: false,
        timestamps: true,
    },
);

CompanySchema.plugin(slug);
CompanySchema.plugin(mongoosePaginate);
