import { CreatePerfectRenovationDto } from './dto/create-perfect-renovation.dto';
import { PerfectRenovationSchema } from "../../schemas/perfect-renovation.schema";
import { PaginateModel } from "mongoose-paginate-v2";
export declare class PerfectRenovationService {
    private model;
    constructor(model: PaginateModel<PerfectRenovationSchema>);
    findById(id: string): Promise<PerfectRenovationSchema>;
    create(createProductDto: CreatePerfectRenovationDto): Promise<PerfectRenovationSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<PerfectRenovationSchema>;
    findAll(queryParams: any): Promise<PerfectRenovationSchema[]>;
    remove(id: string): Promise<PerfectRenovationSchema>;
}
