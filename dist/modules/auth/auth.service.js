"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateToken = exports.AuthService = void 0;
const jwt_1 = require("@nestjs/jwt");
const common_1 = require("@nestjs/common");
const users_service_1 = require("../users/users.service");
const crypto = require("crypto");
const errors_service_1 = require("../../config/errors.service");
let AuthService = class AuthService {
    constructor(usersService, jwtService, error) {
        this.usersService = usersService;
        this.jwtService = jwtService;
        this.error = error;
    }
    async create(user) {
        try {
            return await this.usersService.create(user);
        }
        catch (err) {
            if (err.code === 11000) {
                throw new common_1.HttpException(this.error.get('EMAIL_HAS_ALREADY_USED'), 400);
            }
            throw err;
        }
    }
    async validateUser(payload) {
        try {
            return this.usersService.findOneByEmail(payload.email);
        }
        catch (err) {
            throw err;
        }
    }
    async validateUserById(userId) {
        return this.usersService.findById(userId);
    }
    async signIn(user) {
        return this.generateJWTToken(user);
    }
    generateJWTToken(user) {
        const payload = {};
        payload._id = user._id;
        payload.email = user.email;
        payload.role = user.role;
        return this.jwtService.sign(payload);
    }
    decode(token) {
        return this.jwtService.decode(token);
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [users_service_1.UsersService,
        jwt_1.JwtService,
        errors_service_1.ErrorsService])
], AuthService);
exports.AuthService = AuthService;
const generateToken = () => {
    return crypto.randomBytes(16).toString('hex');
};
exports.generateToken = generateToken;
//# sourceMappingURL=auth.service.js.map