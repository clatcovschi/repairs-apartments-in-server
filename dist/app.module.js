"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const database_module_1 = require("./database/database.module");
const config_1 = require("@nestjs/config");
const mongoose_1 = require("@nestjs/mongoose");
const auth_module_1 = require("./modules/auth/auth.module");
const users_module_1 = require("./modules/users/users.module");
const config_service_1 = require("./config/config.service");
const user_schema_1 = require("./schemas/user.schema");
const email_module_1 = require("./modules/email/email.module");
const core_1 = require("@nestjs/core");
const roles_guard_1 = require("./modules/auth/roles.guard");
const profile_module_1 = require("./modules/profile/profile.module");
const init_service_1 = require("./middlewares/init.service");
const company_module_1 = require("./modules/company/company.module");
const services_module_1 = require("./modules/services/services.module");
const blog_module_1 = require("./modules/blog/blog.module");
const about_us_module_1 = require("./modules/about-us/about-us.module");
const company_advantages_module_1 = require("./modules/company-advantages/company-advantages.module");
const perfect_renovation_module_1 = require("./modules/perfect-renovation/perfect-renovation.module");
const organization_module_1 = require("./modules/organization/organization.module");
const goals_module_1 = require("./modules/goals/goals.module");
const photo_albums_module_1 = require("./modules/photo-albums/photo-albums.module");
const messages_module_1 = require("./modules/messages/messages.module");
const schedule_1 = require("@nestjs/schedule");
const app_service_1 = require("./app.service");
const axios_1 = require("@nestjs/axios");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            config_1.ConfigModule.forRoot({ isGlobal: true }),
            mongoose_1.MongooseModule.forFeature([{ name: 'User', schema: user_schema_1.UserSchema }]),
            auth_module_1.AuthModule,
            users_module_1.UsersModule,
            config_1.ConfigModule,
            database_module_1.DatabaseModule,
            email_module_1.EmailModule,
            profile_module_1.ProfileModule,
            company_module_1.CompanyModule,
            services_module_1.ServicesModule,
            blog_module_1.BlogModule,
            about_us_module_1.AboutUsModule,
            company_advantages_module_1.CompanyAdvantagesModule,
            perfect_renovation_module_1.PerfectRenovationModule,
            organization_module_1.OrganizationModule,
            goals_module_1.GoalsModule,
            photo_albums_module_1.PhotoAlbumsModule,
            messages_module_1.MessagesModule,
            schedule_1.ScheduleModule.forRoot(),
            axios_1.HttpModule
        ],
        controllers: [app_controller_1.AppController],
        providers: [
            app_service_1.AppService,
            config_service_1.ConfigService,
            init_service_1.InitService,
            {
                provide: core_1.APP_GUARD,
                useClass: roles_guard_1.RolesGuard,
            },
        ],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map