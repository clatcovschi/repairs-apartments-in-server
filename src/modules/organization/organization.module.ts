import {Module} from '@nestjs/common';
import {OrganizationService} from './organization.service';
import {OrganizationController} from './organization.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {OrganizationSchema} from "../../schemas/organization.schema";
import {UploadController} from "../upload/upload.controller";
import {UploadService} from "../upload/upload.service";

@Module({
    imports: [MongooseModule.forFeature([{name: 'Organization', schema: OrganizationSchema}])],
    controllers: [OrganizationController, UploadController],
    providers: [OrganizationService, UploadService]
})
export class OrganizationModule {
}
