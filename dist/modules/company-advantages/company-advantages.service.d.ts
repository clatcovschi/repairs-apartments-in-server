import { PaginateModel } from 'mongoose-paginate-v2';
import { CompanyAdvantagesSchema } from "../../schemas/company-advantages.schema";
import { CreateCompanyAdvantageDto } from "./dto/create-company-advantage.dto";
export declare class CompanyAdvantagesService {
    private model;
    constructor(model: PaginateModel<CompanyAdvantagesSchema>);
    findById(id: string): Promise<CompanyAdvantagesSchema>;
    create(createProductDto: CreateCompanyAdvantageDto): Promise<CompanyAdvantagesSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<CompanyAdvantagesSchema>;
    findAll(queryParams: any): Promise<CompanyAdvantagesSchema[]>;
    remove(id: string): Promise<CompanyAdvantagesSchema>;
}
