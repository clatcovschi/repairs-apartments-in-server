/// <reference types="node" />
import { StrictEventEmitter } from 'nest-emitter';
import { EventEmitter } from 'events';
interface AppEvents {
    orderEmail: (cart: any) => void;
}
export declare type EmailEvents = StrictEventEmitter<EventEmitter, AppEvents>;
export {};
