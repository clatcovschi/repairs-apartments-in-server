export declare class CreateCompanyAdvantageDto {
    title: string;
    description: string;
    mainProductImage: object;
}
