import {ApiProperty} from '@nestjs/swagger';
export class CreateBlogDto {
    @ApiProperty()
    title: string;
    @ApiProperty()
    slug: string;
    @ApiProperty()
    miniDescription: string;
    @ApiProperty()
    description: string;
    @ApiProperty()
    mainProductImage: object;
}

