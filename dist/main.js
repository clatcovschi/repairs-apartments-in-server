"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const swagger_1 = require("@nestjs/swagger");
const app_module_1 = require("./app.module");
const config_service_1 = require("./config/config.service");
const bodyParser = require("body-parser");
const helmet = require("helmet");
const common_1 = require("@nestjs/common");
const http_exception_filter_1 = require("./middlewares/http-exception.filter");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule, { cors: true });
    const config = app.get(config_service_1.ConfigService);
    app.use(helmet());
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    app.enableCors();
    app.setGlobalPrefix('api');
    app.useGlobalPipes(new common_1.ValidationPipe());
    swagger_1.SwaggerModule.setup('swagger', app, swagger_1.SwaggerModule.createDocument(app, new swagger_1.DocumentBuilder()
        .setTitle('Repairs-apartments API')
        .setDescription('This is the api description of Repairs-apartments web application')
        .setVersion('1.0')
        .addBearerAuth()
        .setBasePath('api')
        .build()));
    app.useGlobalFilters(new http_exception_filter_1.HttpExceptionFilter());
    await app.listen(config.get('PORT'));
}
bootstrap();
//# sourceMappingURL=main.js.map