import {Controller, Get, Post, Body, Patch, Param, Delete, Query, UseGuards, Put} from '@nestjs/common';
import {PerfectRenovationService} from './perfect-renovation.service';
import {ApiBearerAuth, ApiTags} from "@nestjs/swagger";
import {AuthGuard} from "@nestjs/passport";
import {Roles} from "../../decorators/roles.decorator";
import {PerfectRenovationSchema} from "../../schemas/perfect-renovation.schema";
import {CreatePerfectRenovationDto} from "./dto/create-perfect-renovation.dto";

@ApiTags('PerfectRenovation')
@Controller('perfect-renovation')
export class PerfectRenovationController {
    constructor(private readonly service: PerfectRenovationService) {
    }

    @Get()
    async findAll(@Query() query: any): Promise<PerfectRenovationSchema[]> {
        return this.service.findAll(query);
    }

    @Get('/:id')
    async findById(@Param('id') id: string): Promise<PerfectRenovationSchema> {
        return this.service.findById(id);
    }

    @ApiBearerAuth()
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async create(@Body() createProductDto: CreatePerfectRenovationDto) {
        return this.service.create(createProductDto);
    }

    @ApiBearerAuth()
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async findByIdAndUpdate(@Param('id') id: string, @Body() product): Promise<PerfectRenovationSchema> {
        return this.service.findByIdAndUpdate(id, product);
    }

    @ApiBearerAuth()
    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async remove(@Param('id') id: string) {
        return this.service.remove(id);
    }
}
