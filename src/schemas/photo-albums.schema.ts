import {Document, Schema} from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface PhotoAlbumsSchema extends Document {
    title: string;
    albums: object;
}

export const PhotoAlbumsSchema = new Schema(
    {
        title: {type: String},
        albums: [],
    },
    {
        minimize: false,
        timestamps: true,
    },
);

PhotoAlbumsSchema.plugin(slug);
PhotoAlbumsSchema.plugin(mongoosePaginate);
