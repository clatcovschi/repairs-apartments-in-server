export class ErrorsService {
    private readonly errors: {};

    constructor() {
        this.errors = {
            USER_NOT_FOUND: {
                code: 1,
                message: 'User does not exist or not found',
            },
            EMAIL_HAS_ALREADY_USED: {
                code: 2,
                message: 'This email address has already been used',
            },
            USER_HAS_ALREADY_ACTIVATED: {
                code: 3,
                message: 'User with email is already activated',
            },
            ACTIVATION_TOKEN_INVALID: {
                code: 4,
                message: 'User with such reset token does not exist',
            },
            TOKEN_TIME_EXPIRED: {
                code: 5,
                message: 'Time for reset password has expired',
            },
            PASSWORD_INCORRECT: {
                code: 6,
                message: 'Password is incorrect',
            },
            USER_NOT_ACTIVE: {
                code: 7,
                message: 'User it is not active',
            },
        };
    }

    public get(key: string): string {
        return this.errors[key];
    }
}
