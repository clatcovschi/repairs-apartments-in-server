// Target server hostname or IP address
const TARGET_SERVER_HOST = 'api.pistrix.md';
// Target server username
const TARGET_SERVER_USER = 'root';
// Target server application path
const TARGET_SERVER_APP_PATH = `/${TARGET_SERVER_USER}/app`;

// Your repository
const REPO = 'https://gitlab.com/clatcovschi/repairs-apartments-in-server.git';

module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps: [
        {
            name: 'repairs_server',
            script: 'dist/main.js',
            env: {
                NODE_ENV: 'development',
            },
            env_production: {
                NODE_ENV: 'production',
                PORT: 3000,
            },
        },
    ],

    /**
     * Deployment section
     * http://pm2.keymetrics.io/docs/usage/deployment/
     */
    deploy: {
        production: {
            key: '/root/access.pub',
            host: TARGET_SERVER_HOST,
            ref: 'origin/master',
            repo: REPO,
            ssh_options: 'StrictHostKeyChecking=no',
            path: TARGET_SERVER_APP_PATH,
            'post-deploy': 'npm install --production, npm run start',
        },
    },
};
