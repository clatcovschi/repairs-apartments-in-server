export declare class CreateAboutUsDto {
    miniDescription: string;
    description: string;
}
