import {Injectable} from '@nestjs/common';
import {CreateOrganizationDto} from './dto/create-organization.dto';
import {InjectModel} from "@nestjs/mongoose";
import {PaginateModel} from "mongoose-paginate-v2"
import {OrganizationSchema} from "../../schemas/organization.schema";

@Injectable()
export class OrganizationService {
    constructor(@InjectModel('Organization') private model: PaginateModel<OrganizationSchema>) {
    }

    async findById(id: string): Promise<OrganizationSchema> {
        return await this.model.findById(id).lean();
    }

    async create(createProductDto: CreateOrganizationDto): Promise<OrganizationSchema> {
        return this.model.create(createProductDto);
    }

    async findByIdAndUpdate(id: string, product): Promise<OrganizationSchema> {
        return this.model.findByIdAndUpdate(id, product);
    }


    async findAll(queryParams: any): Promise<OrganizationSchema[]> {
        const query: any = {};
        const options: any = {
            lean: true,
            page: parseInt(queryParams.page, 10) || 1,
            limit: parseInt(queryParams.limit, 10) || 10,
            sort: {createdAt: -1},
        };

        return this.model.paginate(query, options);
    }

    async remove(id: string): Promise<OrganizationSchema> {
        return this.model.findByIdAndDelete(id);
    }
}
