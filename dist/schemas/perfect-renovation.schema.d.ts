import { Document, Schema } from 'mongoose';
export interface PerfectRenovationSchema extends Document {
    title: string;
    renovation: object;
}
export declare const PerfectRenovationSchema: Schema<Document<any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
