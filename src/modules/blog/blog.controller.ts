import {Controller, Get, Post, Body, Patch, Param, Delete, Query, UseGuards, Put} from '@nestjs/common';
import {BlogService} from './blog.service';
import {ApiBearerAuth, ApiTags} from "@nestjs/swagger";
import {AuthGuard} from "@nestjs/passport";
import {Roles} from "../../decorators/roles.decorator";
import {BlogSchema} from "../../schemas/blog.schema";
import {CreateBlogDto} from "./dto/create-blog.dto";

@ApiTags('Blog')
@Controller('blog')
export class BlogController {
    constructor(private readonly blogService: BlogService) {
    }

    @Get()
    async findAll(@Query() query: any): Promise<BlogSchema[]> {
        return this.blogService.findAll(query);
    }

    @Get('/:id')
    async findById(@Param('id') id: string): Promise<BlogSchema> {
        return this.blogService.findById(id);
    }

    @ApiBearerAuth()
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async create(@Body() createProductDto: CreateBlogDto) {
        return this.blogService.create(createProductDto);
    }

    @ApiBearerAuth()
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async findByIdAndUpdate(@Param('id') id: string, @Body() product): Promise<BlogSchema> {
        return this.blogService.findByIdAndUpdate(id, product);
    }

    @ApiBearerAuth()
    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async remove(@Param('id') id: string) {
        return this.blogService.remove(id);
    }
}
