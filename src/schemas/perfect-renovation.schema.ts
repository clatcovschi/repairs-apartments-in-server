import {Document, Schema} from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface PerfectRenovationSchema extends Document {
    title: string;
    renovation: object
}

export const PerfectRenovationSchema = new Schema(
    {
        title: {type: String},
        renovation: [],
    },
    {
        minimize: false,
        timestamps: true,
    },
);

PerfectRenovationSchema.plugin(slug);
PerfectRenovationSchema.plugin(mongoosePaginate);
