import { PerfectRenovationService } from './perfect-renovation.service';
import { PerfectRenovationSchema } from "../../schemas/perfect-renovation.schema";
import { CreatePerfectRenovationDto } from "./dto/create-perfect-renovation.dto";
export declare class PerfectRenovationController {
    private readonly service;
    constructor(service: PerfectRenovationService);
    findAll(query: any): Promise<PerfectRenovationSchema[]>;
    findById(id: string): Promise<PerfectRenovationSchema>;
    create(createProductDto: CreatePerfectRenovationDto): Promise<PerfectRenovationSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<PerfectRenovationSchema>;
    remove(id: string): Promise<PerfectRenovationSchema>;
}
