import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from '../../schemas/user.schema';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    geyMyUserProfile(user: any): Promise<User>;
    findAll(query: any): Promise<User[]>;
    findById(id: string): Promise<User>;
    create(createUserDto: CreateUserDto): Promise<User>;
    findByIdAndDelete(id: string): Promise<User>;
}
