import {Injectable} from '@nestjs/common';
import {CreatePerfectRenovationDto} from './dto/create-perfect-renovation.dto';
import {InjectModel} from "@nestjs/mongoose";
import {PerfectRenovationSchema} from "../../schemas/perfect-renovation.schema";
import {PaginateModel} from "mongoose-paginate-v2"

@Injectable()
export class PerfectRenovationService {
    constructor(@InjectModel('PerfectRenovation') private model: PaginateModel<PerfectRenovationSchema>) {
    }

    async findById(id: string): Promise<PerfectRenovationSchema> {
        return await this.model.findById(id).lean();
    }

    async create(createProductDto: CreatePerfectRenovationDto): Promise<PerfectRenovationSchema> {
        return this.model.create(createProductDto);
    }

    async findByIdAndUpdate(id: string, product): Promise<PerfectRenovationSchema> {
        return this.model.findByIdAndUpdate(id, product);
    }


    async findAll(queryParams: any): Promise<PerfectRenovationSchema[]> {
        const query: any = {};
        const options: any = {
            lean: true,
            page: parseInt(queryParams.page, 10) || 1,
            limit: parseInt(queryParams.limit, 10) || 10,
            sort: {createdAt: -1},
        };

        return this.model.paginate(query, options);
    }

    async remove(id: string): Promise<PerfectRenovationSchema> {
        return this.model.findByIdAndDelete(id);
    }
}
