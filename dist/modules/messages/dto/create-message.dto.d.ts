export declare class CreateMessageDto {
    name: string;
    phone: string;
    mail: string;
    message: string;
}
