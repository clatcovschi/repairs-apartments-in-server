import { ServicesService } from './services.service';
import { CreateServiceDto } from './dto/create-service.dto';
import { ServicesSchema } from "../../schemas/services.schema";
export declare class ServicesController {
    private readonly service;
    constructor(service: ServicesService);
    findAll(query: any): Promise<ServicesSchema[]>;
    findById(id: string): Promise<ServicesSchema>;
    create(createProductDto: CreateServiceDto): Promise<ServicesSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<ServicesSchema>;
    remove(id: string): Promise<ServicesSchema>;
}
