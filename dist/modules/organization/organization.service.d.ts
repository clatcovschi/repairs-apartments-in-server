import { CreateOrganizationDto } from './dto/create-organization.dto';
import { PaginateModel } from "mongoose-paginate-v2";
import { OrganizationSchema } from "../../schemas/organization.schema";
export declare class OrganizationService {
    private model;
    constructor(model: PaginateModel<OrganizationSchema>);
    findById(id: string): Promise<OrganizationSchema>;
    create(createProductDto: CreateOrganizationDto): Promise<OrganizationSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<OrganizationSchema>;
    findAll(queryParams: any): Promise<OrganizationSchema[]>;
    remove(id: string): Promise<OrganizationSchema>;
}
