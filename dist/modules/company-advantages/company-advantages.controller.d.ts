import { CompanyAdvantagesService } from "./company-advantages.service";
import { CompanyAdvantagesSchema } from "../../schemas/company-advantages.schema";
import { CreateCompanyAdvantageDto } from "./dto/create-company-advantage.dto";
export declare class CompanyAdvantagesController {
    private readonly service;
    constructor(service: CompanyAdvantagesService);
    findAll(query: any): Promise<CompanyAdvantagesSchema[]>;
    findById(id: string): Promise<CompanyAdvantagesSchema>;
    create(createProductDto: CreateCompanyAdvantageDto): Promise<CompanyAdvantagesSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<CompanyAdvantagesSchema>;
    remove(id: string): Promise<CompanyAdvantagesSchema>;
}
