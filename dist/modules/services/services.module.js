"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServicesModule = void 0;
const common_1 = require("@nestjs/common");
const services_service_1 = require("./services.service");
const services_controller_1 = require("./services.controller");
const mongoose_1 = require("@nestjs/mongoose");
const upload_controller_1 = require("../upload/upload.controller");
const errors_service_1 = require("../../config/errors.service");
const upload_service_1 = require("../upload/upload.service");
const services_schema_1 = require("../../schemas/services.schema");
let ServicesModule = class ServicesModule {
};
ServicesModule = __decorate([
    common_1.Module({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: 'Services', schema: services_schema_1.ServicesSchema }])],
        controllers: [services_controller_1.ServicesController, upload_controller_1.UploadController],
        providers: [services_service_1.ServicesService, errors_service_1.ErrorsService, upload_service_1.UploadService]
    })
], ServicesModule);
exports.ServicesModule = ServicesModule;
//# sourceMappingURL=services.module.js.map