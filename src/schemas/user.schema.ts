import {Document, Schema} from 'mongoose';
import {genSaltSync, hashSync} from 'bcrypt';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface User extends Document {
    readonly email: string;
    password: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly description: string;
    isActive: boolean;
    activationToken: string;
    passwordResetExpire: Date;
    passwordResetToken: string;
    role: string;

    hashPassword(password: string): boolean;
}

export const UserSchema = new Schema(
    {
        firstName: {type: String},
        lastName: {type: String},
        description: {type: String},
        email: {type: String, required: true, unique: true},
        isActive: {type: Boolean, default: true},
        activationToken: {type: String},
        passwordResetToken: {type: String},
        passwordResetExpire: {type: Date},
        password: {type: String, required: true},
        role: {type: String, enum: ['admin', 'user'], default: 'admin'},
    },
    {
        timestamps: true,
    },
);

UserSchema.pre<User>('save', function (next: any): any {
    const saltRounds = 10;
    const user = this;
    if (this.isModified('password') || this.isNew) {
        const salt = genSaltSync(saltRounds);
        user.password = hashSync(user.password, salt);
        next();
    } else {
        return next();
    }
});

UserSchema.plugin(mongoosePaginate);
