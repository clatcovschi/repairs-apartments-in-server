import {ApiProperty} from "@nestjs/swagger";

export class CreateMessageDto {
    @ApiProperty()
    name: string;
    @ApiProperty()
    phone: string;
    @ApiProperty()
    mail: string;
    @ApiProperty()
    message: string;
}
