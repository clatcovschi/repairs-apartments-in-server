"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_paginate_v2_1 = require("mongoose-paginate-v2");
const nest_emitter_1 = require("nest-emitter");
const crypto = require("crypto");
let UsersService = class UsersService {
    constructor(userModel, emitter) {
        this.userModel = userModel;
        this.emitter = emitter;
        this.generateToken = () => {
            return crypto.randomBytes(16).toString('hex');
        };
    }
    async getMyProfile(id) {
        return this.userModel.findById(id).select('-password -passwordResetToken -passwordResetExpire -activationToken');
    }
    async findAll(queryParams) {
        const query = {};
        const options = {
            page: parseInt(queryParams.page, 10) || 1,
            limit: parseInt(queryParams.limit, 10) || 10,
            select: '-password',
        };
        if (queryParams._sort) {
            options.sort = {
                [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
            };
        }
        if (Object.keys(queryParams).length === 0) {
            return this.userModel.find({}).select('-password');
        }
        if (queryParams.email) {
            query.email = { $regex: queryParams.email, $options: 'i' };
        }
        return this.userModel.paginate(query, options);
    }
    async findById(id) {
        return this.userModel.findById(id).select('-password');
    }
    async findOneByEmail(email) {
        return this.userModel.findOne({ email });
    }
    async create(user) {
        user.activationToken = this.generateToken();
        return await this.userModel.create(user);
    }
    async findByIdAndUpdate(id, userData) {
        return this.userModel.findByIdAndUpdate(id, userData, { new: true }).select('-password');
    }
    async findByIdAndDelete(id) {
        return this.userModel.findByIdAndDelete(id).select('-password');
    }
    async findOneByKeyForPasswordReset(key) {
        return this.userModel.findOne({ passwordResetToken: key }).select('-password');
    }
    async findOneByEmailForActivation(email, activationToken) {
        return this.userModel.findOne({ email, activationToken }).select('-password');
    }
    async activateUser(id) {
        return this.userModel.findByIdAndUpdate(id, { $set: { isActive: true, activationToken: null } });
    }
};
UsersService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('User')),
    __param(1, nest_emitter_1.InjectEventEmitter()),
    __metadata("design:paramtypes", [typeof (_a = typeof mongoose_paginate_v2_1.PaginateModel !== "undefined" && mongoose_paginate_v2_1.PaginateModel) === "function" ? _a : Object, Object])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map