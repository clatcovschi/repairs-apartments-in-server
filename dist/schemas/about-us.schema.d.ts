import { Document, Schema } from 'mongoose';
export interface AboutUsSchema extends Document {
    miniDescription: string;
    description: string;
}
export declare const AboutUsSchema: Schema<Document<any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
