import {Controller, Get, Param, Query, Req, Res} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";
import {ImageProxyQuery} from "./utils/dto/ImageProxy.query";
import {Cron, CronExpression} from '@nestjs/schedule';
import {AppService} from "./app.service";

@ApiTags('Public')
@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {
    }

    @Get('public/company/images/:fileId')
    async serveCompanyImages(
        @Param('fileId') fileId,
        @Res() res,
        @Req() req: any,
        @Query() query: ImageProxyQuery,
    ): Promise<any> {
        res.sendFile(fileId, {root: 'public/company/images'});
    }

    @Get('public/company-advantages/images/:fileId')
    async serveCompanyAdvantagesImages(
        @Param('fileId') fileId,
        @Res() res,
        @Req() req: any,
        @Query() query: ImageProxyQuery,
    ): Promise<any> {
        res.sendFile(fileId, {root: 'public/company-advantages/images'});
    }

    @Get('public/organization/images/:fileId')
    async serveOrganizationImages(
        @Param('fileId') fileId,
        @Res() res,
        @Req() req: any,
        @Query() query: ImageProxyQuery,
    ): Promise<any> {
        res.sendFile(fileId, {root: 'public/organization/images'});
    }

    @Get('public/service/images/:fileId')
    async serveServiceImages(
        @Param('fileId') fileId,
        @Res() res,
        @Req() req: any,
        @Query() query: ImageProxyQuery,
    ): Promise<any> {
        res.sendFile(fileId, {root: 'public/service/images'});
    }

    @Get('public/photo-album/images/:fileId')
    async servePhotoAlbumImages(
        @Param('fileId') fileId,
        @Res() res,
        @Req() req: any,
        @Query() query: ImageProxyQuery,
    ): Promise<any> {
        res.sendFile(fileId, {root: 'public/photo-album/images'});
    }

    @Get('public/blog/images/:fileId')
    async serveBlogImages(
        @Param('fileId') fileId,
        @Res() res,
        @Req() req: any,
        @Query() query: ImageProxyQuery,
    ): Promise<any> {
        res.sendFile(fileId, {root: 'public/blog/images'});
    }

    @Cron(CronExpression.EVERY_WEEK)
    async handleCron() {
        const filterEntities = [['company', 'company', true, false, false], ['company-advantages', 'company-advantages', false, false, false], ['blog','blog',false, false, false], ['services', 'service',false, false, false], ['organization','organization',false, true, false], ['photo-albums', 'photo-album',false, false, true]];
        filterEntities.map(el => this.appService.filterImages(el[0], el[1], el[2], el[3], el[4]));
    }
}
