import {Document, Schema} from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface AboutUsSchema extends Document {
    miniDescription: string;
    description: string;
}


export const AboutUsSchema = new Schema(
    {
        miniDescription: {type: String},
        description: {type: String},
    },
    {
        minimize: false,
        timestamps: true,
    },
);

AboutUsSchema.plugin(slug);
AboutUsSchema.plugin(mongoosePaginate);
