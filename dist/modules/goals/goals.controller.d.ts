import { GoalsService } from './goals.service';
import { CreateGoalDto } from './dto/create-goal.dto';
import { GoalsSchema } from "../../schemas/goals.schema";
export declare class GoalsController {
    private readonly service;
    constructor(service: GoalsService);
    findAll(query: any): Promise<GoalsSchema[]>;
    findById(id: string): Promise<GoalsSchema>;
    create(createProductDto: CreateGoalDto): Promise<GoalsSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<GoalsSchema>;
    remove(id: string): Promise<GoalsSchema>;
}
