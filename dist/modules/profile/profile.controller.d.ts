import { UsersService } from '../users/users.service';
import { ProfileService } from './profile.service';
import { UpdateProfileDto } from "./dto/update-profile.dto";
import { CreateProfileDto } from "./dto/create-profile.dto";
export declare class ProfileController {
    private userService;
    private profileService;
    constructor(userService: UsersService, profileService: ProfileService);
    activateEmail(email: string, token: string): Promise<void>;
    resetPasswordInit(resetPasswordDto: UpdateProfileDto): Promise<any>;
    resetPasswordFinish(resetPasswordFinishDto: CreateProfileDto): Promise<any>;
}
