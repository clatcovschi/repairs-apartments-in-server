"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolesGuard = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const auth_service_1 = require("./auth.service");
let RolesGuard = class RolesGuard {
    constructor(reflector, authService) {
        this.reflector = reflector;
        this.authService = authService;
    }
    async canActivate(context) {
        let user;
        const role = this.reflector.get('role', context.getHandler());
        if (!role) {
            return true;
        }
        const token = this.getToken(context);
        const payload = await this.authService.decode(token);
        if (payload) {
            user = await this.authService.validateUserById(payload._id);
        }
        return user && user.role && isAllowed(role, user.role);
    }
    getToken(executionContext) {
        const request = executionContext.switchToHttp().getRequest();
        if (request.headers.authorization) {
            return request.headers.authorization.split(' ')[1];
        }
        else {
            return '';
        }
    }
};
RolesGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [core_1.Reflector, auth_service_1.AuthService])
], RolesGuard);
exports.RolesGuard = RolesGuard;
function isAllowed(routeRole, userRole) {
    return routeRole.indexOf(userRole) > -1;
}
//# sourceMappingURL=roles.guard.js.map