import {Injectable} from '@nestjs/common';
import {CreateServiceDto} from './dto/create-service.dto';
import {InjectModel} from "@nestjs/mongoose";
import {PaginateModel} from 'mongoose-paginate-v2';
import {ServicesSchema} from "../../schemas/services.schema";

@Injectable()
export class ServicesService {
    constructor(@InjectModel('Services') private model: PaginateModel<ServicesSchema>) {
    }

    async findById(id: string): Promise<ServicesSchema> {
        return  await this.model.findById(id).lean();
    }

    async create(createProductDto: CreateServiceDto): Promise<ServicesSchema> {
        return this.model.create(createProductDto);
    }

    async findByIdAndUpdate(id: string, product): Promise<ServicesSchema> {
        return this.model.findByIdAndUpdate(id, product);
    }


    async findAll(queryParams: any): Promise<ServicesSchema[]> {
        const query: any = {};
        const options: any = {
            lean: true,
            page: parseInt(queryParams.page, 10) || 1,
            limit: parseInt(queryParams.limit, 10) || 10,
            sort: {createdAt: -1},
        };

        return this.model.paginate(query, options);
    }

    async remove(id: string): Promise<ServicesSchema> {
        return this.model.findByIdAndDelete(id);
    }
}

