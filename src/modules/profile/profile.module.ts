import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from '../../schemas/user.schema';
import { ProfileController } from './profile.controller';
import { ProfileService } from './profile.service';
import { UsersService } from '../users/users.service';
import { ConfigService } from '../../config/config.service';
import { ErrorsService } from '../../config/errors.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'User', schema: UserSchema }])],
  controllers: [ProfileController],
  providers: [ConfigService, ProfileService, UsersService, ErrorsService],
  exports: [ProfileService],
})
export class ProfileModule {}
