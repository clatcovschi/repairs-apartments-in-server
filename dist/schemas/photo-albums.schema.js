"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhotoAlbumsSchema = void 0;
const mongoose_1 = require("mongoose");
const slug = require("mongoose-slug-updater");
const mongoosePaginate = require("mongoose-paginate-v2");
exports.PhotoAlbumsSchema = new mongoose_1.Schema({
    title: { type: String },
    albums: [],
}, {
    minimize: false,
    timestamps: true,
});
exports.PhotoAlbumsSchema.plugin(slug);
exports.PhotoAlbumsSchema.plugin(mongoosePaginate);
//# sourceMappingURL=photo-albums.schema.js.map