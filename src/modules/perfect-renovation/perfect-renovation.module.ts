import {Module} from '@nestjs/common';
import {PerfectRenovationService} from './perfect-renovation.service';
import {PerfectRenovationController} from './perfect-renovation.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {PerfectRenovationSchema} from "../../schemas/perfect-renovation.schema";

@Module({
    imports: [MongooseModule.forFeature([{name: 'PerfectRenovation', schema: PerfectRenovationSchema}])],
    controllers: [PerfectRenovationController],
    providers: [PerfectRenovationService]
})
export class PerfectRenovationModule {
}
