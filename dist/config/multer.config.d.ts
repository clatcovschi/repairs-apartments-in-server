export declare const multerConfig: {
    CompanyUpload: string;
    CompanyAdvantagesUpload: string;
    OrganizationUpload: string;
    ServiceUpload: string;
    PhotoAlbumUpload: string;
    BlogUpload: string;
};
export declare const fileSizeLimits: {
    images: number;
    documents: number;
};
export declare const CompanyOptions: {
    limits: {
        fileSize: number;
    };
    fileFilter: (req: any, file: any, cb: any) => void;
    storage: any;
};
export declare const CompanyAdvantagesOptions: {
    limits: {
        fileSize: number;
    };
    fileFilter: (req: any, file: any, cb: any) => void;
    storage: any;
};
export declare const OrganizationOptions: {
    limits: {
        fileSize: number;
    };
    fileFilter: (req: any, file: any, cb: any) => void;
    storage: any;
};
export declare const ServiceOptions: {
    limits: {
        fileSize: number;
    };
    fileFilter: (req: any, file: any, cb: any) => void;
    storage: any;
};
export declare const PhotoAlbumOptions: {
    limits: {
        fileSize: number;
    };
    fileFilter: (req: any, file: any, cb: any) => void;
    storage: any;
};
export declare const BlogOptions: {
    limits: {
        fileSize: number;
    };
    fileFilter: (req: any, file: any, cb: any) => void;
    storage: any;
};
