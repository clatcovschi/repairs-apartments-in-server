import {Controller, Get, Post, Body, Param, Delete, Query, UseGuards, Put} from '@nestjs/common';
import {MessagesService} from './messages.service';
import {CreateMessageDto} from './dto/create-message.dto';
import {ApiBearerAuth, ApiTags} from "@nestjs/swagger";
import {AuthGuard} from "@nestjs/passport";
import {Roles} from "../../decorators/roles.decorator";
import {MessagesSchema} from "../../schemas/messages.schema";
import {EmailService} from "../email/email.service";

@ApiTags('Messages')
@Controller('messages')
export class MessagesController {
    constructor(private readonly service: MessagesService,
                private readonly email: EmailService) {
    }


    @ApiBearerAuth()
    @Get()
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async findAll(@Query() query: any): Promise<MessagesSchema[]> {
        return this.service.findAll(query);
    }

    @ApiBearerAuth()
    @Get('/email')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async findAllMessages(@Query() query: any): Promise<MessagesSchema[]> {
        return this.service.findAll(query);
    }

    @ApiBearerAuth()
    @Get('/:id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async findById(@Param('id') id: string): Promise<MessagesSchema> {
        return this.service.findById(id);
    }


    @Post()
    async create(@Body() createProductDto: CreateMessageDto) {
        return this.service.create(createProductDto);
    }

    @Post('/email')
    async sendEmail(@Body() createProductDto: CreateMessageDto) {
        return this.service.sendEmail(createProductDto);
    }

    @ApiBearerAuth()
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async findByIdAndUpdate(@Param('id') id: string, @Body() product): Promise<MessagesSchema> {
        return this.service.findByIdAndUpdate(id, product);
    }

    @ApiBearerAuth()
    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async remove(@Param('id') id: string) {
        return this.service.remove(id);
    }

    @ApiBearerAuth()
    @Delete('/email/:id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async removeEmail(@Param('id') id: string) {
        return this.service.remove(id);
    }



}
