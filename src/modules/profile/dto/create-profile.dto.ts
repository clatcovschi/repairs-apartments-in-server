import { IsDefined, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateProfileDto {
    @IsNotEmpty()
    @ApiProperty()
    public key: string;
    @IsDefined()
    @ApiProperty()
    public newPassword: string;
}
