export declare class CreateCompanyDto {
    name: string;
    phone: string;
    slug: string;
    mail: string;
    coordinates: string;
    networksDescription: string;
    whatsapp: string;
    whatsappDescription: string;
    vk: string;
    instagram: string;
    facebook: string;
    workingHours: string;
    principalColor: string;
    secondaryColor: string;
    slogan: string;
    servicePageDescription: string;
    photoAlbumsPageDescription: string;
    mainProductImage: object;
    mainProductImageContacts: object;
}
