import { Model } from 'mongoose';
import { User } from '../schemas/user.schema';
import { OnModuleInit } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
export declare class InitService implements OnModuleInit {
    private userModel;
    private config;
    private readonly logger;
    constructor(userModel: Model<User>, config: ConfigService);
    onModuleInit(): Promise<void>;
    create(): Promise<User>;
}
